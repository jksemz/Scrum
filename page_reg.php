<!DOCTYPE HTML>
<?php
session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    include_once 'hub.php';

/*
    Filename: page_reg.php
    Author: Jethro Kyle Sempio
    
    Script Type: User Interface
    Description: 
    User sign up UI
*/

    $tst = 'Signup';
    if(isset($_POST['btn_reg'])){
        $user = new User();
        $user->name = $_POST['i_name'];
        $user->email = $_POST['i_email'];
        $user->pwd = $_POST['i_pwd'];
        createUser($user);
        $user = login($user->email, $user->pwd);
        if($user->user_id != 0){
            $_SESSION['userid'] = $user->userid;
            header('location:page_user.php');
        }
    }
?>
    <html>
        <head>
            <title>Scrum</title>
        </head>
        <body>
            <?php 
                navBar();
            ?>
            
            <br>
            <center>
            <div class='w3-card-4 sc-white' 
            style=' margin:20px; 
                    padding:20px;
                    width:350px;'
            >
                <h3>Signup</h3>
                <form method='POST'>
                    <input type='text' name='i_name' placeholder='Username' class='sc-grey sc-txt-drkgrey sc-flat'style='display:block; padding:10px'>
                    <br>
                    <input type='email' name='i_email' placeholder='Email' class='sc-grey sc-txt-drkgrey sc-flat'style='display:block; padding:10px'>
                    <br>
                    <input type='password' name='i_pwd' placeholder='Password' class='sc-grey sc-txt-drkgrey sc-flat'style='display:block; padding:10px'>
                    <br>
                    <button class='w3-button w3-green' name='btn_reg'>Signup</button>
                </form>
            </div>
            </center>
        </body>
    </html>