<!DOCTYPE HTML>
<?php
    session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    include_once 'hub.php';
    $user = new User();

/*
    Filename: index.php
    Author: Jethro Kyle Sempio
    
    Script Type: UI, index
    Description: 
    Home page and login UI
*/

    if(isset($_GET['logout']) && $_GET['logout']){
        session_unset();
		session_destroy();
		$_GET['logout'] = false;
		header('location:index.php');
    }

    if(isset($_SESSION['userid'])){
        header('location:page_user.php');
    }

    if(isset($_POST['btn_login'])){
        $user = login($_POST['i_email'], $_POST['i_pwd']);
        if($user->user_id != 0){
            $_SESSION['userid'] = $user->user_id;
            header('location:page_user.php');
        }
    }
?>
    <html>
        <head>
            <title>Scrum</title>
        </head>
        <body>
            <?php 
                navBar();
            ?>
            <span>Scrum just got easier!</span>
            <br>
            <div class='w3-card-4 sc-white' 
            style=' margin:20px; 
                    padding:20px;
                    width:350px;
                    float:right;'
            >
                <h3>Login</h3>
                <form method='POST'>
                    <input type='email' name='i_email' placeholder='Email' class='sc-grey sc-txt-drkgrey sc-flat'style='display:block; padding:10px'>
                    <br>
                    <input type='password' name='i_pwd' placeholder='Password' class='sc-grey sc-txt-drkgrey sc-flat'style='display:block; padding:10px'>
                    <br>
                    <button class='w3-button w3-green' name='btn_login'>Login</button>
                    <a class='w3-button w3-green' href='page_reg.php'>Signup</a>
                </form>
            </div>
        </body>
    </html>