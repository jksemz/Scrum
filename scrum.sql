-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 15, 2018 at 12:39 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scrum`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `acceptInvite` (IN `i_inviteid` INT(8))  BEGIN
	UPDATE proj_invite SET status = 'D' WHERE invite_id = i_inviteid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delProj` (IN `i_projid` INT(8), IN `i_userid` INT(8))  BEGIN
    DECLARE isOwner INT(1);
    SET isOwner = (SELECT COUNT(user_id) FROM proj
        WHERE proj_id = i_projid AND user_id = i_userid);

    IF isOwner = 1 THEN
        DELETE FROM backlog WHERE proj_id = i_projid;
        DELETE FROM proj_member WHERE proj_id = i_projid;
        DELETE FROM proj_invite WHERE proj_id = i_projid;
        DELETE FROM proj WHERE proj_id = i_projid;
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getEndDate` (IN `i_projid` INT(8))  BEGIN
    SELECT DATE_ADD(start_date, INTERVAL proj_numWeeks WEEK) as 'end_date' FROM proj WHERE proj_id = i_projid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getToday` ()  BEGIN
    SELECT current_date as 'today';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `kickMember` (IN `i_userid` INT(8), IN `i_projid` INT(8))  BEGIN
    UPDATE backlog SET user_id = NULL WHERE user_id = i_userid AND proj_id = i_projid;
    DELETE FROM proj_member WHERE user_id = i_userid AND proj_id = i_projid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pointsLeftOnDate` (IN `i_userid` INT(8), IN `i_projid` INT(8), IN `i_date` VARCHAR(10))  BEGIN
    DECLARE points INT(8);
    SET points = (SELECT SUM(backlog_size.bck_points) + 0 FROM backlog
    INNER JOIN backlog_size ON backlog.bck_size = backlog_size.bck_size
    WHERE 
        status = 'D' AND
        done_date <= STR_TO_DATE(i_date,'%Y-%m-%d') AND
        user_id = i_userid AND
        proj_id = i_projid);
    CALL userMaxPoints(i_userid,i_projid,@maxPoints);
    IF points IS NULL THEN
        SELECT @maxPoints AS 'pointsLeft';
    END IF;
    IF points IS NOT NULL THEN
        SELECT @maxPoints - points AS 'pointsLeft';
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `projBckCount` (IN `i_projid` INT(8))  BEGIN
    SELECT COUNT(bck_id) as 'bckCount' FROM backlog WHERE proj_id = i_projid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `projBckDoneCount` (IN `i_projid` INT(8))  BEGIN
    SELECT COUNT(bck_id) as 'bckCount' FROM backlog WHERE proj_id = i_projid AND status = 'D';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rejectInvite` (IN `i_inviteid` INT(8))  BEGIN
    DELETE FROM proj_invite WHERE invite_id = i_inviteid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sendInvite` (IN `i_email` VARCHAR(30), IN `i_projid` INT(8))  BEGIN
DECLARE r_userid    INT(8);
    DECLARE isInvited   INT(1);
    DECLARE isMembered  INT(1);
    DECLARE isOwner     INT(1);
    
    SET r_userid = (SELECT user_id FROM user 
    WHERE email = i_email);
    SET isInvited = (SELECT COUNT(user_id) FROM proj_invite 
    WHERE user_id = r_userid AND proj_id = i_projid AND status = 'O');
    SET isMembered = (SELECT COUNT(user_id) FROM proj_member
    WHERE user_id = r_userid AND proj_id = i_projid);
    SET isOwner = (SELECT COUNT(user_id) FROM proj
        WHERE user_id = r_userid AND proj_id = i_projid);
    
    IF isInvited = 0 AND isMembered = 0 AND isOwner = 0 THEN
INSERT INTO proj_invite
        (
                user_id ,
                proj_id ,
                status
            )
            VALUES
            (
                r_userid,
                i_projid,
                'O'
            );
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `taskDone` (IN `i_bckid` INT(8))  BEGIN
    UPDATE backlog SET status = 'D', done_date = current_date() WHERE bck_id = i_bckid AND user_id IS NOT NULL;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `taskUndone` (IN `i_bckid` INT(8))  BEGIN
    UPDATE backlog SET status = 'O', done_date = NULL WHERE bck_id = i_bckid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `userDonePoints` (IN `i_userid` INT(8), IN `i_projid` INT(8))  BEGIN
    SELECT SUM(backlog_size.bck_points) FROM backlog
INNER JOIN backlog_size ON backlog.bck_size = backlog_size.bck_size
    WHERE user_id = i_userid AND proj_id = i_projid AND status = 'D';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `userMaxPoints` (IN `i_userid` INT(8), IN `i_projid` INT(8), OUT `o_maxPoints` INT(8))  BEGIN
    SELECT SUM(backlog_size.bck_points) FROM backlog
INNER JOIN backlog_size ON backlog.bck_size = backlog_size.bck_size
    WHERE user_id = i_userid AND proj_id = i_projid INTO o_maxPoints;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `backlog`
--

CREATE TABLE `backlog` (
  `bck_id` int(8) NOT NULL,
  `proj_id` int(8) NOT NULL,
  `user_id` int(8) DEFAULT NULL,
  `bck_size` char(3) NOT NULL,
  `bck_title` varchar(60) NOT NULL,
  `bck_desc` varchar(150) NOT NULL,
  `sprint_no` int(2) NOT NULL DEFAULT '2',
  `status` char(1) NOT NULL DEFAULT 'O',
  `done_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backlog`
--

INSERT INTO `backlog` (`bck_id`, `proj_id`, `user_id`, `bck_size`, `bck_title`, `bck_desc`, `sprint_no`, `status`, `done_date`) VALUES
(1, 5, 1, 'XL', 'Dev ECU Firmware', 'Firmware Modules to be Done:\r\n- Engine Control\r\n- ABS Control\r\n- E-Steering Control', 1, 'D', '2018-05-13'),
(3, 5, 4, 'XL', 'Model Y Commerical', 'Length: 2:40\r\nSetting: Cupertino', 1, 'O', NULL),
(4, 3, 2, 'S', 'Create Value Preposition', 'no comments', 1, 'O', NULL),
(5, 4, 3, 'S', 'Dev: Falcon 9 - Block 5 Blueprint', '- use autocad', 2, 'D', '2017-12-21'),
(26, 1, 1, 'S', 'Problem Validation', '- Interview\r\n- Make Statistic', 0, 'D', '2018-05-08'),
(27, 1, 1, 'M', 'Dev Landing Page', '- use dreamweaver\r\n- 1 page only', 1, 'O', NULL),
(29, 1, NULL, 'XL', 'Code Front-end', 'HTML\r\nCSS\r\nJS', 2, 'O', NULL),
(30, 1, NULL, 'XL', 'Code Back-end', 'PHP\r\nJava', 2, 'O', NULL),
(32, 1, 16, 'XS', 'Concept Solutions', '- Go to Library for inspirations\r\n- Google and google', 0, 'D', '2018-05-15'),
(33, 1, 16, 'M', 'Competition Assessment', 'Ask Jet for format', 0, 'D', '2018-05-07'),
(34, 1, 15, 'XS', 'Customer Empathy Map', 'Format saved on google drive', 0, 'D', '2018-05-09'),
(35, 1, 1, 'M', 'Explore coding trends', '- Web\r\n- Mobile\r\n- IoT', 0, 'D', '2018-05-07'),
(36, 1, 15, 'S', 'Design Landing Page', 'Use adobe XD', 1, 'O', NULL),
(37, 1, 16, 'M', 'DB ERD', 'use dia', 1, 'O', NULL),
(38, 1, 16, 'L', 'Func Documentation', 'No comment', 1, 'D', '2018-05-14'),
(39, 1, 1, 'M', 'Class Diagram', 'use Dia', 1, 'D', '2018-05-14'),
(40, 1, 15, 'L', 'Use Case Diagram', 'use Dia', 1, 'D', '2018-05-13'),
(41, 1, 1, 'L', 'Dev Crude Login', 'Platform: web', 1, 'D', '2018-05-14'),
(42, 1, NULL, 'L', 'Explore Firebase', 'Google it', 1, 'O', NULL),
(43, 1, NULL, 'XL', 'Explore ML', 'Google it', 1, 'O', NULL),
(44, 1, NULL, 'XL', 'Finalize Pitch Deck', 'Description', 2, 'O', NULL),
(45, 1, NULL, 'M', 'Digitize Pitch Deck', 'Powerpoint or Preezy', 1, 'O', NULL),
(46, 1, 16, 'S', 'Draft Pitch Deck', 'Good for 12 slides deck', 0, 'D', '2018-05-10');

-- --------------------------------------------------------

--
-- Table structure for table `backlog_size`
--

CREATE TABLE `backlog_size` (
  `bck_size` varchar(3) NOT NULL,
  `bck_points` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backlog_size`
--

INSERT INTO `backlog_size` (`bck_size`, `bck_points`) VALUES
('L', 13),
('M', 8),
('S', 3),
('XL', 21),
('XS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `proj`
--

CREATE TABLE `proj` (
  `proj_id` int(8) NOT NULL,
  `user_id` int(8) NOT NULL,
  `proj_name` varchar(30) NOT NULL,
  `proj_numWeeks` int(2) NOT NULL,
  `start_date` date NOT NULL,
  `sprint_count` int(2) NOT NULL,
  `sprint_numWeeks` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proj`
--

INSERT INTO `proj` (`proj_id`, `user_id`, `proj_name`, `proj_numWeeks`, `start_date`, `sprint_count`, `sprint_numWeeks`) VALUES
(1, 1, 'MGT - Tinder Clone', 3, '2018-05-06', 3, 1),
(3, 2, 'Mac OS 11', 50, '2018-08-15', 25, 2),
(4, 3, 'Space X BFR Prototype', 100, '2017-10-25', 50, 5),
(5, 3, 'Tesla Model Y', 36, '2018-01-17', 12, 3);

--
-- Triggers `proj`
--
DELIMITER $$
CREATE TRIGGER `moveBckSpr` AFTER UPDATE ON `proj` FOR EACH ROW BEGIN
	DECLARE spr INT(8);
	IF NEW.sprint_count < OLD.sprint_count THEN
        SET spr = NEW.sprint_count - 1;
        UPDATE backlog SET sprint_no = spr
        WHERE sprint_no > NEW.sprint_count;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `proj_invite`
--

CREATE TABLE `proj_invite` (
  `invite_id` int(8) NOT NULL,
  `proj_id` int(8) NOT NULL,
  `user_id` int(8) NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proj_invite`
--

INSERT INTO `proj_invite` (`invite_id`, `proj_id`, `user_id`, `status`) VALUES
(1, 3, 1, 'D'),
(4, 3, 5, 'A'),
(8, 3, 15, 'D'),
(11, 5, 15, 'D'),
(12, 4, 15, 'O'),
(20, 1, 15, 'D'),
(21, 1, 5, 'O'),
(22, 1, 16, 'D');

--
-- Triggers `proj_invite`
--
DELIMITER $$
CREATE TRIGGER `addMember` AFTER UPDATE ON `proj_invite` FOR EACH ROW BEGIN
		IF NEW.status = 'D' 
        THEN
				INSERT INTO proj_member
                	(
                        user_id	,
                        proj_id
                    )
                    VALUES
                    (
                        NEW.user_id,
                        NEW.proj_id
                    );
        END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `proj_member`
--

CREATE TABLE `proj_member` (
  `user_id` int(8) NOT NULL,
  `proj_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proj_member`
--

INSERT INTO `proj_member` (`user_id`, `proj_id`) VALUES
(5, 3),
(1, 5),
(2, 5),
(4, 5),
(15, 3),
(15, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(8) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `email`, `password`) VALUES
(1, 'Jethro Kyle Sempio', 'jksemz@gmail.com', 'thelinuxdev'),
(2, 'Steve Jobs', 'steve@apple.com', 'thinkdifferent'),
(3, 'Elon Musk', 'emusk@tesla.com', 'mars'),
(4, 'James Cameron', 'jc@gmail.com', 'titanic'),
(5, 'Johnny Ive', 'john_ive@apple.com', 'applehipster'),
(15, 'Nicole', 'nicole@gmail.com', '1234'),
(16, 'Mhel Ely', 'mhel@gmail.com', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backlog`
--
ALTER TABLE `backlog`
  ADD PRIMARY KEY (`bck_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `proj_id` (`proj_id`),
  ADD KEY `bck_size` (`bck_size`);

--
-- Indexes for table `backlog_size`
--
ALTER TABLE `backlog_size`
  ADD PRIMARY KEY (`bck_size`);

--
-- Indexes for table `proj`
--
ALTER TABLE `proj`
  ADD PRIMARY KEY (`proj_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `proj_invite`
--
ALTER TABLE `proj_invite`
  ADD PRIMARY KEY (`invite_id`),
  ADD KEY `proj_id` (`proj_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `proj_member`
--
ALTER TABLE `proj_member`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `proj_id` (`proj_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backlog`
--
ALTER TABLE `backlog`
  MODIFY `bck_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `proj`
--
ALTER TABLE `proj`
  MODIFY `proj_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `proj_invite`
--
ALTER TABLE `proj_invite`
  MODIFY `invite_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `backlog`
--
ALTER TABLE `backlog`
  ADD CONSTRAINT `backlog_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `backlog_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `backlog_ibfk_3` FOREIGN KEY (`proj_id`) REFERENCES `proj` (`proj_id`),
  ADD CONSTRAINT `backlog_ibfk_4` FOREIGN KEY (`bck_size`) REFERENCES `backlog_size` (`bck_size`);

--
-- Constraints for table `proj`
--
ALTER TABLE `proj`
  ADD CONSTRAINT `proj_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `proj_invite`
--
ALTER TABLE `proj_invite`
  ADD CONSTRAINT `proj_invite_ibfk_1` FOREIGN KEY (`proj_id`) REFERENCES `proj` (`proj_id`),
  ADD CONSTRAINT `proj_invite_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `proj_member`
--
ALTER TABLE `proj_member`
  ADD CONSTRAINT `proj_member_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `proj_member_ibfk_2` FOREIGN KEY (`proj_id`) REFERENCES `proj` (`proj_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
