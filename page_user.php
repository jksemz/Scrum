<!DOCTYPE HTML>
<?php
session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    include_once 'hub.php';
    $user = new User();

/*
    Filename: page_user.php
    Author: Jethro Kyle Sempio
    
    Script Type: User Interface
    Description: 
    User's personal UI, displays included projects and pending invites
*/
    if(isset($_SESSION['projid']))
        unset($_SESSION['projid']);

    if(isset($_SESSION['userid']))
        $user = getUserRoot($_SESSION['userid']);
    else
        header('location:index.php'); 

    if(isset($_POST['btn_reg'])){
        $user = new User();
        $user->name = $_POST['i_name'];
        $user->email = $_POST['i_email'];
        $user->pwd = $_POST['i_pwd'];
        createUser($user);
    }

    if(isset($_POST['btnInvAc']))
        acceptInvite($_POST['btnInvAc']);
    if(isset($_POST['btnInvRj']))
        rejectInvite($_POST['btnInvRj']);

    if(isset($_POST['btnKick']))
        kickMember($_SESSION['userid'], $_POST['btnKick']);

    if(isset($_POST['btnDel']))
        delProj($_POST['btnDel'], $_SESSION['userid']);
    
    if(isset($_POST['btnEdit']))
        header('location:page_projEdit.php?projid='.$_POST['btnEdit']);
?>
<?php
    function navBarUser(){ ?>
        <nav>
            <form method='POST'>
                <div class='w3-bar sc-green w3-card-2' style='color:white;'>
                    <a class='w3-bar-item w3-button' href='index.php'>Scrum!</a>
                    <?php  
                        if(isset($_SESSION['userid']))
                            echo '<a href="index.php?logout=true" class="w3-bar-item w3-right w3-button" name="btn_logout">LOGOUT</a>';
                    ?>
            </form>
                    <span class='w3-button w3-green w3-bar-item w3-right' onclick='openSidebar()'>INVITES</span>
                </div>
        </nav>
<?php
    } ?>

    <html>
        <head>
            <title>Scrum</title>
        </head>
        <body>
            <?php 
                navBar();
            ?>
            <br>
            <table style='height:550px'>
                <tr>
                    <td height='100%'>
                        <div class='w3-card-2 sc-white' style ='margin:20px; width:950px;padding:10px;height:100%'>
                            <h1>Hello <?php echo $user->name ?></h1>
                            <a class='w3-button w3-green' href='page_projAdd.php'>Add Project</a>
                            <br>
                                <div>
                                    <div>
                                        <span id='btnTb0' onclick='changeTab(0)' class='w3-button w3-green w3-half'>My Projects</span>
                                        <span id='btnTb1' onclick='changeTab(1)' class='w3-button w3-green w3-half'>Involved Projects</span>
                                    <div>
                                    <div id='tb0'>
                                        <?php 
                                            projList(getProjByUser($user->user_id));
                                        ?>
                                    </div>
                                    <div id='tb1'>
                                        <?php
                                            projList(getProjByMember($user->user_id));
                                        ?>
                                    </div>
                                <div>
                        </div>
                    </td>
                    <td height='100%'>
                        <div id="sbar_invite" class="w3-grey w3-card-4 w3-bar-block" style="
                            margin-top:20px;
                            width:300px;
                            height:550px;
                            right:0;
                            padding-top:10px; 
                            padding-bottom:15px;
                        ">
                            <center>
                                <h3>Invites</h3>
                            </center>
                                    <div style='overflow-y:scroll;overflow-x:hidden;height:96%;margin-right:-28px;'>
                                        <form method='POST'>
                                            <?php inviteList(getInvites($_SESSION['userid'])); ?>
                                        </form>
                                    </div>
                        </div>
                    </td>
                </tr>
            </table>
            <script>
                changeTab(0);
                function changeTab(tabNo){
                    for(var x=0; x < 2; x++){
                        document.getElementById('tb'+x).style.display = 'none';
                        document.getElementById('btnTb'+x).style.textDecoration = 'none';
                    }
                    
                    document.getElementById('tb'+tabNo).style.display = 'block';
                    document.getElementById('btnTb'+tabNo).style.textDecoration = 'underline';

                }
            </script>
        </body>
    </html>