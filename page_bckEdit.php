<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'hub.php';
$user = new User();

/*
Filename: page_bckEdit.php
Author: Jethro Kyle Sempio

Script Type: User Interface
Description: 
Edit backlog UI
*/
    $bck = '';
    if(isset($_SESSION['userid']) && isset($_SESSION['projid']) && isset($_GET['bckid'])){
        if(validateProjMember($_SESSION['userid'], $_SESSION['projid']) || validateProjOwner($_SESSION['userid'], $_SESSION['projid'])){
            $user = getUserRoot($_SESSION['userid']);
            $bck = getBacklog($_GET['bckid']);
            $members = getProjMembers($_SESSION['projid']);
            $proj = getProj($_SESSION['projid']);
            $bckSizes = getBckSizes();
            if($bck instanceof Err)
                echo $bck->errMsg;
        }
        else
            header('location:index.php'); 
    }
    else
        header('location:index.php'); 

    if(isset($_POST['btnBckEdit'])){
        $updBck = new Backlog();
        $updBck->bck_title = $_POST['i_bckTitle'];
        $updBck->bck_desc = strip_tags(trim(htmlspecialchars($_POST['i_bckDesc'])));
        $updBck->bck_size = $_POST['i_bckSize'];
        $updBck->user_id = $_POST['i_asgnUser'];
        $updBck->sprint_no = $_POST['i_sprNo'];
        $updBck->bck_id = $_GET['bckid'];
        $err = editBacklog($updBck);
        
        if($err instanceof Err)
            echo $err->errMsg;
        else
            header('location:page_bck.php?bckid='.$bck->bck_id);
        print_r($updBck);
    }

    if(isset($_POST['backBck'])){
        header('location:page_bck.php?bckid='.$bck->bck_id);
    }

?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Scrum</title>
    </head>
    <?php
        navBar();
    ?>
    <body style='overflow:hidden'>
        <div class='w3-card sc-white' style='margin:50px;height:100%'>
            <form method='POST'>
                <button name='backBck' class='w3-button w3-green'>X Cancel</button>
                <button name='btnBckEdit' class='w3-button w3-green' style='margin-left:30px;float:right'>Save Changes</button>
            <div style='padding:30px;height:65%;'>
                <h2>Edit Backlog</h2>
                <br>
                <div class='w3-half'>
                    <input type='text' value='<?php echo $bck->bck_title; ?>' name='i_bckTitle' placeholder='Title' style='sc-white sc-txt-drkgrey'>
                    <br><br>

                    <textarea name='i_bckDesc' rows='5' cols='25' style='sc-white sc-txt-drkgrey'><?php echo $bck->bck_desc; ?></textarea>
                    <br><br>

                    <p>Backlog Size</p>
                    <select name='i_bckSize' value='<?php echo $bck->bck_size; ?>' style='sc-white sc-txt-drkgrey'>
                        <?php
                            for($x=0; $x < count($bckSizes); $x++){
                                echo "<option value='".$bckSizes[$x]->bck_size."'>";
                                echo $bckSizes[$x]->bck_size." - ".$bckSizes[$x]->bck_points." pts";
                                echo "</option>";
                            }
                        ?>
                    </select>
                </div>

                <div class='w3-half'>
                    <p>Assigned User</p>
                    <select name='i_asgnUser' style='sc-white sc-txt-drkgrey'>
                        <option value='0'>None</option>
                        <?php
                            $owner = getProjOwner($proj->proj_id);
                            if($owner->user_id == $user->user_id){
                                echo "<option value='".$user->user_id."'>Me";
                                echo "</option>";
                            } else{
                                echo "<option value='".$user->user_id."'>Me";
                                echo "</option>";

                                echo "<option value='".$owner->user_id."'>";
                                echo $owner->name;
                                echo "</option>";
                            }

                            for($x=0; $x < count($members); $x++){
                                if($members[$x]->user_id != $_SESSION['userid']) {
                                    echo "<option value='".$members[$x]->user_id."'>";
                                    echo $members[$x]->name;
                                    echo "</option>";
                                }
                            }
                        ?>
                    </select>

                    <p>Sprint Number</p>
                    <select name='i_sprNo' style='sc-white sc-txt-drkgrey'>
                    <?php
                        for($x=0; $x < $proj->sprint_count; $x++){
                            echo "<option value='".$x."'>";
                            echo $x+1;
                            echo "</option>";
                        }
                    ?>
                    </select>
                </div>
            </div>
            <br><br>
            </form>
            <span id='btnDel' onclick='delBck()' class='w3-button w3-green'>Delete Backlog</span>
        </div>
        <script>
            document.getElementsByName('i_bckSize')[0].value = '<?php echo $bck->bck_size; ?>';
            document.getElementsByName('i_sprNo')[0].value = '<?php echo $bck->sprint_no; ?>';

            if(<?php if($bck->user_id == null) echo 1; else echo 0;  ?> == 0)
                document.getElementsByName('i_asgnUser')[0].value = '<?php echo $bck->user_id; ?>';
            else
                document.getElementsByName('i_asgnUser')[0].value = 0;

            var confirmDelete = false;

            function delBck(){
                if(confirmDelete)
                    window.location = "page_proj.php?delbck=<?php echo $bck->bck_id; ?>&projid=<?php echo $_SESSION['projid']; ?>";
                else{
                    document.getElementById('btnDel').innerHTML = 'Sure? Click again';
                    confirmDelete = true;
                }
            }
        </script>
    </body>
</html>