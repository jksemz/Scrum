<!DOCTYPE HTML>
<?php
session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    include_once 'hub.php';
    $user = new User();
    
/*
    Filename: page_proj.php
    Author: Jethro Kyle Sempio
    
    Script Type: User Interface
    Description: 
    Scrum main project management dashboard
*/

    if(isset($_SESSION['userid']) && isset($_GET['projid'])){
        if(validateProjMember($_SESSION['userid'], $_GET['projid']) || validateProjOwner($_SESSION['userid'], $_GET['projid'])){
            $user = getUserRoot($_SESSION['userid']);
            $members = getProjMembers($_GET['projid']);
            array_push($members, getProjOwner($_GET['projid']));
            $proj = getProj($_GET['projid']);
            $_SESSION['projid'] = $_GET['projid'];

            if(isset($_GET['delbck'])){
                deleteBacklog($_GET['delbck']);
                unset($_GET['delbck']);
            }

            for($x=0; $x < count($members); $x++){
                if($members[$x]->user_id == $user->user_id){
                    unset($members[$x]);
                    $members = array_values($members);
                    break;
                }
            }
        }
        else
            header('location:index.php'); 
    }
    else
        header('location:index.php'); 

    if(isset($_POST['btn_goBckAdd'])){
        $_SESSION['projid'] = $proj->proj_id;
        header('location:page_bckAdd.php');
    }
    
?>
    <html>
        <head>
            <title>Scrum</title>
        </head>
        <body>
            <?php 
                navBar();
            ?>
            <br>
            <center>
                <div style='margin-left:20px;margin-right:20px'>
                    <div style='display:inline-table;float:left'>
                        <span class='w3-button w3-green' onclick='changePage("-")'><</span>
                        <span id='sprInd' style='font-size:18px'>Sprint</span>
                        <span class='w3-button w3-green' onclick='changePage("+")'>></span>
                    </div>

                    <div style='display:inline-table;float:right'>
                        <a href='page_member.php?projid=<?php echo $_GET['projid']; ?>' class='w3-button w3-green'>Members</a>
                        <a href='page_stat.php?projid=<?php echo $_GET['projid']; ?>' class='w3-button w3-green'>Stats</a>
                    </div>
                    <div class='w3-center'>
                        <span style='font-size:24px;'><?php echo $proj->proj_name ?></span>
                        <br>
                        <img src='3rd_party/img/ic_watch_later_black_18dp.png'>
                        <span id='dayCount' style='font-size:12px;'>   
                        </span>
                        <script>
                            var today = new Date('<?php echo getToday(); ?>');
                            var endDate = new Date('<?php echo getProjEndDate($_GET['projid']); ?>');
                            document.getElementById('dayCount').innerHTML = ((endDate.getTime() - today.getTime()) / 86400000) + " days left";
                        </script>
                    <div>
                </div>
            </center>
            <table>
                <tr>
                    <td>
                    <div class='w3-card-2 sc-white' style ='margin:20px;padding:10px;width:950px;display:inline-table'>
                        <?php
                            for($x = 0; $x < $proj->sprint_count; $x++){ ?>
                                <div id = '<?php echo "spr".$x ?>' style='overflow-x:scroll; overflow-y:hidden; white-space: nowrap; height:50%;width:900px'>
                        <?php
                                $userBck = getAssignedBacklogs($user->user_id, $_GET['projid'], $x);
                                //$bck = getBacklogs($user->user_id, $_GET['projid']);
                                bckList($user, $userBck);
                                for($y = 0; $y < count($members); $y++){
                                    $bck = getAssignedBacklogs($members[$y]->user_id, $_GET['projid'], $x);
                                    bckList($members[$y], $bck);
                                }
                                echo '</div>';
                            }
                        ?>
                    </div>
                 </td>

                <td valign='top'>
                    <div class='w3-card sc-grey' style='top:0; width:300px; margin-top:20px; display:inline-table'>
                        <div style='margin:15px'>
                            <h4 style='left:0; display:inline'>Backlog</h4>
                            <form style='margin-top:-30px' method='POST'>
                                <button name='btn_goBckAdd' class='w3-right w3-button w3-green'>ADD</button>
                            </form>
                        </div>
                        <br><br>
                            <div style='height:470px;margin:10px;overflow-y:scroll;overflow-x:hidden'>
                                <?php
                                    for($x=0; $x < $proj->sprint_count; $x++){
                                        echo "<div id='stsh".$x."' style='height:100'>";
                                                $noUserBck = getBacklogsNoUser($_GET['projid'], $x);
                                                foreach($noUserBck as $item)
                                                    bckCard($item);
                                        echo "</div>";
                                    }
                                ?>
                            </div>
                    </div>
                </td>
            </tr>
        </table>
        <script>
            var page = 0;
            var sprCount = <?php echo $proj->sprint_count; ?>;
            changePage('-');


            function changePage(flag){
                switch(flag){
                    case '+':
                        if(page < sprCount && page+1 < sprCount)
                            page++;
                    break;
                    case '-':
                        if(page > -1 && page-1 > -1)
                            page--;
                    break;
                }
                document.getElementById('sprInd').innerHTML = 'Sprint '+(page+1);
                document.getElementById('spr'+page).style.display = 'block';
                document.getElementById('stsh'+page).style.display = 'block';
                for(let x=0; x < sprCount; x++){
                    if(x != page){
                        document.getElementById('spr'+x).style.display = 'none';
                        document.getElementById('stsh'+x).style.display = 'none';
                    }
                }
            }
         </script>
    </body>
 </html>