<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'hub.php';
$user = new User();

/*
Filename: page_bckAdd.php
Author: Jethro Kyle Sempio

Script Type: User Interface
Description: 
Create backlog UI
*/
    $err = '';
    if(isset($_SESSION['userid']) && isset($_SESSION['projid'])){
        if(validateProjMember($_SESSION['userid'], $_SESSION['projid']) || validateProjOwner($_SESSION['userid'], $_SESSION['projid'])){
            $user = getUserRoot($_SESSION['userid']);
            $members = getProjMembers($_SESSION['projid']);
            $proj = getProj($_SESSION['projid']);
            $bckSizes = getBckSizes();
        }
        else
            header('location:index.php'); 
    }
    else
        header('location:index.php'); 

    if(isset($_POST['backProj'])){
        unset($_SESSION['projid']);
        header('location:page_proj.php?projid='.$proj->proj_id);
    }

    if(isset($_POST['btnBckAdd'])){
        $bck = new Backlog();
        $bck->bck_title = $_POST['i_bckTitle'];
        $bck->bck_desc = strip_tags(trim(htmlspecialchars($_POST['i_bckDesc'])));;
        $bck->bck_size = $_POST['i_bckSize'];
        $bck->proj_id = $proj->proj_id;
        $bck->user_id = $_POST['i_asgnUser'];
        $bck->sprint_no = $_POST['i_sprNo'];
        createBacklog($bck);
        unset($_SESSION['projid']);
        header('location:page_proj.php?projid='.$proj->proj_id);
        print_r($bck);
    }

    if($err instanceof Err)
        echo $err->errMsg;
    
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Scrum</title>
    </head>
    <?php
        navBar();
    ?>
    <body style='overflow:hidden'>
        <div class='w3-card sc-white' style='margin:50px;height:100%'>
            <form method='POST'>
                <button name='backProj' class='w3-button w3-green'>< Back to Project</button>
            <div style='padding:30px;height:65%;'>
                <h2>Add Backlog</h2>
                <br>
                <div class='w3-half'>
                    <input type='text' name='i_bckTitle' placeholder='Title' style='sc-white sc-txt-drkgrey'>
                    <br><br>
                    <textarea name='i_bckDesc' rows='5' cols='25' style='sc-white sc-txt-drkgrey'>Description</textarea>
                    <br><br>
                    <p>Backlog Size</p>
                    <select name='i_bckSize' style='sc-white sc-txt-drkgrey'>
                        <?php
                            for($x=0; $x < count($bckSizes); $x++){
                                echo "<option value='".$bckSizes[$x]->bck_size."'>";
                                echo $bckSizes[$x]->bck_size." - ".$bckSizes[$x]->bck_points." pts";
                                echo "</option>";
                            }
                        ?>
                    </select>
                </div>

                <div class='w3-half'>
                    <p>Assigned User</p>
                    <select name='i_asgnUser' style='sc-white sc-txt-drkgrey'>
                        <option value='0'>None</option>
                        <?php
                            $owner = getProjOwner($proj->proj_id);
                            if($owner->user_id == $user->user_id){
                                echo "<option value='".$user->user_id."'>Me";
                                echo "</option>";
                            } else{
                                echo "<option value='".$user->user_id."'>Me";
                                echo "</option>";

                                echo "<option value='".$owner->user_id."'>";
                                echo $owner->name;
                                echo "</option>";
                            }

                            for($x=0; $x < count($members); $x++){
                                if($members[$x]->user_id != $_SESSION['userid']) {
                                    echo "<option value='".$members[$x]->user_id."'>";
                                    echo $members[$x]->name;
                                    echo "</option>";
                                }
                            }
                        ?>
                    </select>

                    <p>Sprint Number</p>
                    <select name='i_sprNo' style='sc-white sc-txt-drkgrey'>
                    <?php
                        for($x=0; $x < $proj->sprint_count; $x++){
                            echo "<option value='".$x."'>";
                            echo $x+1;
                            echo "</option>";
                        }
                    ?>
                    </select>
                </div>
            </div>
            <button name='btnBckAdd' class='w3-button w3-green' style='margin-left:30px'>ADD</button>
            </form>
        </div>
    </body>
</html>