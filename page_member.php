<!DOCTYPE HTML>
<?php
session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    include_once 'hub.php';
    $user = new User();

/*
    Filename: page_member.php
    Author: Jethro Kyle Sempio
    
    Script Type: User Interface
    Description: 
    Scrum project member dashboard
*/

    if(isset($_SESSION['userid']) && isset($_GET['projid'])){
        if(validateProjMember($_SESSION['userid'], $_GET['projid']) || validateProjOwner($_SESSION['userid'], $_GET['projid'])){
            $user = getUserRoot($_SESSION['userid']);
            $members = getProjMembers($_GET['projid']);
            array_push($members, getProjOwner($_GET['projid']));
            $proj = getProj($_GET['projid']);

            for($x=0; $x < count($members); $x++){
                if($members[$x]->user_id == $user->user_id){
                    unset($members[$x]);
                    $members = array_values($members);
                    break;
                }
            }
        }
        else
            header('location:index.php'); 
    }
    else
        header('location:index.php'); 

    if(isset($_POST['backProj'])){
        header('location:page_proj.php?projid='.$proj->proj_id);
    }

    if(isset($_POST['btnSndInv']))
        sendInvite($_POST['i_email'], $_GET['projid']);

    if(isset($_POST['btnKick'])){
        kickMember($_POST['btnKick'], $_GET['projid']);
        header('location:page_member.php?projid='.$proj->proj_id);
    }
?>
<html>
    <head>
        <title>Scrum</title>
    </head>
    <body>
        <?php 
            navBar();
        ?>
        <br>
        <form method='POST'>
            <div class='sc-white' style='margin:30px;height:550px;'>
                <button name='backProj' class='w3-button w3-green'>< Back to Project</button>        
                <div style='padding:20px'>
                    <h2>Project Team</h2>
                    <div style='display:overflow;height:80%; width:40%'>
                        <?php
                            userCard($user,$_GET['projid']);
                            for($x=0; $x < count($members); $x++)
                                userCard($members[$x], $_GET['projid']);
                        ?>
                    <div>

                    <p>Invite</p>
                    <input type='email' name='i_email' placeholder='Email' class='sc-grey sc-txt-drkgrey' style='padding:10px'>
                    <button name='btnSndInv' class='w3-button w3-green'>Send Invite</button>
                </div>

            </div>
        </form>
    </body>
</html>