<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ERROR);
include_once 'classes.php';
/*
    Filename: util_db.php
    Author: Jethro Kyle Sempio
    
    Script Type: Utility
    Description: 
    Central utility class for database queries
*/
    function getConn(){
        $ip = 'localhost';
        $db = 'scrum';
        $user = 'root';
        $pwd = 'uniteitall';
        return new mysqli($ip, $user, $pwd, $db);
    }
    
    function createUser($user){
        try{
            $conn = getConn();
            $sql =  "INSERT user(name, email, password) VALUES(".
                    "'".$user->name."',".
                    "'".$user->email."',".
                    "'".$user->pwd."')";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getUser($userid){
        try{
            $conn = getConn();
            $user = new User();
            $sql = "SELECT user_id, name, email FROM user WHERE user_id ='".$userid."'";
            $result = mysqli_query($conn, $sql);
            $user = new User();
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $user->user_id = $row['user_id'];
                $user->name = $row['name'];
                $user->email = $row['email'];
            } else
                $user = false;
            mysqli_close($conn);
            return $user;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function login($email, $pwd){
        try{
            $conn = getConn();
            $user = new User();
            $sql = "SELECT * FROM user WHERE email ='".$email."' AND password ='".$pwd."'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $user->user_id = $row['user_id'];
                $user->name = $row['name'];
                $user->email = $row['email'];
                $user->pwd = $row['password'];
            }
            mysqli_close($conn);
            return $user;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getUserRoot($userid){
        $conn = getConn();
        $user = new User();
        $sql = "SELECT * FROM user WHERE user_id ='".$userid."'";
        $result = mysqli_query($conn, $sql);
        if(mysqli_num_rows($result) > 0){
            $row = mysqli_fetch_assoc($result);
            $user->user_id = $row['user_id'];
            $user->name = $row['name'];
            $user->email = $row['email'];
            $user->pwd = $row['password'];
        }
        mysqli_close($conn);
        return $user;
    }

    function createProj($proj){
        try{
            $conn = getConn();
            $sql =  "INSERT proj(user_id, proj_name, proj_numWeeks, start_date, sprint_count, sprint_numWeeks) VALUES(".
                    "'".$proj->user_id."',".
                    "'".$proj->proj_name."',".
                    "'".$proj->proj_numWeeks."',".
                    "STR_TO_DATE('".$proj->startDate."','%Y-%m-%d'),".
                    "'".$proj->sprint_count."',".
                    "'".$proj->sprint_numWeeks."')";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function editProj($proj){
        try{
            $conn = getConn();
            $sql =  "UPDATE proj SET ".
                    "proj_name = '".$proj->proj_name."',".
                    "proj_numWeeks = '".$proj->proj_numWeeks."',".
                    "start_date = STR_TO_DATE('".$proj->startDate."','%Y-%m-%d'),".
                    "sprint_count = '".$proj->sprint_count."',".
                    "sprint_numWeeks = '".$proj->sprint_numWeeks."' ".
                    "WHERE proj_id = '".$proj->proj_id."'";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }
    
    function delProj($projid, $userid){
        try{
            $conn = getConn();
            $sql = "CALL delProj($projid, $userid)";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProj($projid){
        try{
            $conn = getConn();
            $sql = "SELECT * FROM proj WHERE proj_id='".$projid."'";
            $result = mysqli_query($conn, $sql);
            $proj = new Proj();
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $proj->proj_id = $row['proj_id'];
                $proj->user_id = $row['user_id'];
                $proj->proj_name = $row['proj_name'];
                $proj->proj_numWeeks = $row['proj_numWeeks'];
                $proj->startDate = $row['start_date'];
                $proj->sprint_count = $row['sprint_count'];
                $proj->sprint_numWeeks = $row['sprint_numWeeks'];
            }
            mysqli_close($conn);
            return $proj;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProjByUser($userid){
        try{
            $conn = getConn();
            $sql = "SELECT * FROM proj WHERE user_id='".$userid."'";
            $result = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($result);
            $projList = array();
            if($count > 0){
                for($x=0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new Proj();
                    $itm->proj_id = $row['proj_id'];
                    $itm->user_id = $row['user_id'];
                    $itm->proj_name = $row['proj_name'];
                    $itm->proj_numWeeks = $row['proj_numWeeks'];
                    $itm->startDate = $row['start_date'];
                    $itm->sprint_count = $row['sprint_count'];
                    $itm->sprint_numWeeks = $row['sprint_numWeeks'];
                    $projList[$x] = $itm;
                }
            }
            mysqli_close($conn);
            return $projList;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProjByMember($userid){
        try{
            $conn = getConn();
            $sql = "SELECT proj.proj_id, proj.user_id, proj_name, proj_numWeeks, start_date, sprint_count, sprint_numWeeks FROM proj INNER JOIN proj_member ON proj.proj_id = proj_member.proj_id WHERE proj_member.user_id='".$userid."'";
            $result = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($result);
            $projList = array();
            if($count > 0){
                for($x=0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new Proj();
                    $itm->proj_id = $row['proj_id'];
                    $itm->user_id = $row['user_id'];
                    $itm->proj_name = $row['proj_name'];
                    $itm->proj_numWeeks = $row['proj_numWeeks'];
                    $itm->startDate = $row['start_date'];
                    $itm->sprint_count = $row['sprint_count'];
                    $itm->sprint_numWeeks = $row['sprint_numWeeks'];
                    $projList[$x] = $itm;
                }
            }
            mysqli_close($conn);
            return $projList;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProjName($projid){
        try{
            $conn = getConn();
            $sql = "SELECT proj_name FROM proj WHERE proj_id='".$projid."'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                return $row['proj_name'];
            }
            mysqli_close($conn);
            return '';
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProjOwnerName($projid){
        try{
            $conn = getConn();
            $sql = "SELECT name FROM user WHERE user_id = (SELECT user_id FROM proj WHERE proj_id ='".$projid."')";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                return $row['name'];
            }
            mysqli_close($conn);
            return '';
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProjOwner($projid){
        try{
            $conn = getConn();
            $sql = "SELECT user_id, name, email FROM user WHERE user_id = (SELECT user_id FROM proj WHERE proj_id ='".$projid."')";
            $result = mysqli_query($conn, $sql);
            $user = new User();
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $user->user_id = $row['user_id'];
                $user->name = $row['name'];
                $user->email = $row['email'];
            }
            mysqli_close($conn);
            return $user;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProjEndDate($projid){
        try{
            $conn = getConn();
            $sql = "CALL getEndDate($projid)";
            $result = mysqli_query($conn, $sql);
            mysqli_close($conn);

            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                return $row['end_date'];
            }
            return '0000-00-00';
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getToday(){
        try{
            $conn = getConn();
            $sql = "CALL getToday()";
            $result = mysqli_query($conn, $sql);
            mysqli_close($conn);

            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                return $row['today'];
            }
            return '0000-00-00';
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getInvites($userid){
        try{
            $conn = getConn();
            $sql = "SELECT * FROM proj_invite WHERE user_id='".$userid."' AND status = 'O'";
            $result = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($result);
            $inviteList = array();
            if($count > 0){
                for($x = 0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new Proj_Invite();
                    $itm->invite_id = $row['invite_id'];
                    $itm->proj_id = $row['proj_id'];
                    $itm->user_id = $row['user_id'];
                    $itm->status = $row['status'];
                    $inviteList[$x] = $itm;
                }
            }
            mysqli_close($conn);
            return $inviteList;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function acceptInvite($inviteid){
        try{
            $conn = getConn();
            $sql = "CALL acceptInvite($inviteid)";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function sendInvite($email, $projid){
        try{
            $conn = getConn();
            $sql = "CALL sendInvite('$email',$projid)";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function rejectInvite($inviteid){
        try{
            $conn = getConn();
            $sql = "CALL rejectInvite($inviteid)";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function kickMember($userid, $projid){
        try{
            $conn = getConn();
            $sql = "CALL kickMember($userid, $projid)";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function createBacklog($bck){
        try{
            $conn = getConn();
            if($bck->user_id != 0){
                $sql =  "INSERT backlog(proj_id, user_id, bck_size, bck_title, bck_desc, sprint_no) VALUES(".
                        "'".$bck->proj_id."',".
                        "'".$bck->user_id."',".
                        "'".$bck->bck_size."',".
                        "'".$bck->bck_title."',".
                        "'".$bck->bck_desc."',".
                        "'".$bck->sprint_no."')";
            } else{
                $sql =  "INSERT backlog(proj_id, bck_size, bck_title, bck_desc, sprint_no) VALUES(".
                        "'".$bck->proj_id."',".
                        "'".$bck->bck_size."',".
                        "'".$bck->bck_title."',".
                        "'".$bck->bck_desc."',".
                        "'".$bck->sprint_no."')";
            }
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function editBacklog($bck){
        try{
            $conn = getConn();
            if($bck->user_id > 0){
                $sql = "UPDATE backlog SET ".
                "user_id = '".$bck->user_id."',".
                "bck_size = '".$bck->bck_size."',".
                "bck_title = '".$bck->bck_title."',".
                "bck_desc = '".$bck->bck_desc."',".
                "sprint_no = '".$bck->sprint_no."' ".
                "WHERE bck_id = '".$bck->bck_id."'";
            } else{
                $sql = "UPDATE backlog SET ".
                "user_id = NULL,".
                "bck_size = '".$bck->bck_size."',".
                "bck_title = '".$bck->bck_title."',".
                "bck_desc = '".$bck->bck_desc."',".
                "sprint_no = '".$bck->sprint_no."' ".
                "WHERE bck_id = '".$bck->bck_id."'";
            }
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function backlogDone($bckid){
        try{
            $conn = getConn();
            $sql = "CALL taskDone($bckid)";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function backlogUndone($bckid){
        try{
            $conn = getConn();
            $sql = "CALL taskUndone($bckid)";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function deleteBacklog($bckid){
        try{
            $conn = getConn();
            $sql = "DELETE FROM backlog WHERE bck_id = '".$bckid."'";
            mysqli_query($conn, $sql);
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getBacklog($bckid){
        try{
            $conn = getConn();
            $sql = "SELECT * FROM backlog WHERE bck_id ='".$bckid."'";
            $result = mysqli_query($conn, $sql);
            $bck = new Backlog();
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $bck->bck_id = $row['bck_id'];
                $bck->proj_id = $row['proj_id'];
                $bck->user_id = $row['user_id'];
                $bck->bck_size = $row['bck_size'];
                $bck->bck_title = $row['bck_title'];
                $bck->bck_desc = $row['bck_desc'];
                $bck->sprint_no = $row['sprint_no'];
                $bck->status = $row['status'];
                $bck->done_date = $row['done_date'];
            }
            return $bck;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    // Retrive project  backlogs belong to the user
    function getAssignedBacklogs($userid, $projid, $sprNo){
        try{
            $conn = getConn();
            $sql = "SELECT * FROM backlog WHERE proj_id ='".$projid."' AND user_id = '".$userid."' AND sprint_no = '".$sprNo."'";
            $result = mysqli_query($conn, $sql);
            $bckList = array();
            $count = mysqli_num_rows($result);
            if($count > 0){
                for($x = 0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new Backlog();
                    $itm->bck_id = $row['bck_id'];
                    $itm->proj_id = $row['proj_id'];
                    $itm->user_id = $row['user_id'];
                    $itm->bck_size = $row['bck_size'];
                    $itm->bck_title = $row['bck_title'];
                    $itm->bck_desc = $row['bck_desc'];
                    $itm->sprint_no = $row['sprint_no'];
                    $itm->status = $row['status'];
                    $itm->done_date = $row['done_date'];
                    $bckList[$x] = $itm;
                }
            }
            return $bckList;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    // Retrive project backlogs not belong to the user
    function getBacklogs($userid, $projid, $sprNo){
        $bckList = array();
        try{
            $conn = getConn();
            $sql = "SELECT * FROM backlog WHERE proj_id ='".$projid."' AND user_id != '".$userid."' AND sprint_no = '".$sprNo."'";
            $result = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($result);
            if($count > 0){
                for($x = 0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new Backlog();
                    $itm->bck_id = $row['bck_id'];
                    $itm->proj_id = $row['proj_id'];
                    $itm->user_id = $row['user_id'];
                    $itm->bck_size = $row['bck_size'];
                    $itm->bck_title = $row['bck_title'];
                    $itm->bck_desc = $row['bck_desc'];
                    $itm->sprint_no = $row['sprint_no'];
                    $itm->status = $row['status'];
                    $itm->done_date = $row['done_date'];
                    $bckList[$x] = $itm;
                }
            }
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
        return $bckList;
    }

    function getBacklogsNoUser($projid, $sprNo){
        $bckList = array();
        try{
            $conn = getConn();
            $sql = "SELECT * FROM backlog WHERE user_id IS NULL AND proj_id ='".$projid."' AND sprint_no ='".$sprNo."'";
            $result = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($result);
            $bckList = array();
            if($count > 0){
                for($x = 0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new Backlog();
                    $itm->bck_id = $row['bck_id'];
                    $itm->proj_id = $row['proj_id'];
                    $itm->bck_size = $row['bck_size'];
                    $itm->bck_title = $row['bck_title'];
                    $itm->bck_desc = $row['bck_desc'];
                    $itm->sprint_no = $row['sprint_no'];
                    $itm->status = $row['status'];
                    $itm->done_date = $row['done_date'];
                    $bckList[$x] = $itm;
                }
            }
            return $bckList;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
        return $bckList;
    }

    function userMaxPoints($userid, $projid){
        try{
            $conn = getConn();
            $sql = "CALL userMaxPoints($userid,$projid,@mx)";
            $result_a = mysqli_query($conn,$sql);
            $sql = "SELECT @mx";
            $result_b = mysqli_query($conn,$sql);
            mysqli_close($conn);
            if($result_b){
                $row = mysqli_fetch_assoc($result_b);
                return $row['@mx'];
            }
            return 0;
            mysqli_close($conn);
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function pointsLeftOnDate($userid, $projid, $date){
        try{
            $conn = getConn();
            $sql = "CALL pointsLeftOnDate($userid, $projid, '$date')";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                return $row['pointsLeft'];
            }
            return 0;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function projBckCount($projid){
        try{
            $conn = getConn();
            $sql = "CALL projBckCount($projid)";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                return $row['bckCount'];
            }
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function projBckDoneCount($projid){
        try{
            $conn = getConn();
            $sql = "CALL projBckDoneCount($projid)";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                return $row['bckCount'];
            }
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    // Return true if user is a member of project
    function validateProjMember($userid, $projid){
        try{
            $conn = getConn();
            $sql = "SELECT user_id FROM proj_member WHERE user_id ='".$userid."' AND proj_id ='".$projid."'";
            $result = mysqli_query($conn, $sql);
            $flag = false;
            if(mysqli_num_rows($result) > 0)
                $flag = true;
            mysqli_close($conn);
            return $flag;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getProjMembers($projid){
        try{
            $conn = getConn();
            $sql = "SELECT user.user_id, name, email FROM user INNER JOIN proj_member ON user.user_id = proj_member.user_id WHERE proj_member.proj_id ='".$projid."'";
            $result = mysqli_query($conn, $sql);
            $memberList = array();
            $count = mysqli_num_rows($result);
            if($count > 0){
                for($x=0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new User();
                    $itm->user_id = $row['user_id'];
                    $itm->name = $row['name'];
                    $itm->email = $row['email'];
                    $memberList[$x] = $itm;
                }
            }
            mysqli_close($conn);
            return $memberList;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function validateProjOwner($userid, $projid){
        try{
            $conn = getConn();
            $sql = "SELECT user_id FROM proj WHERE user_id ='".$userid."' AND proj_id ='".$projid."'";
            $result = mysqli_query($conn, $sql);
            $flag = false;
            if(mysqli_num_rows($result) > 0)
                $flag = true;
            mysqli_close($conn);
            return $flag;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function emailExists($email){
        try{
            $conn = getConn();
            $sql = "SELECT email FROM user WHERE email ='".$email."'";
            $result = mysqli_result($conn, $sql);
            $flag = false;
            if(mysqli_num_rows($result) > 0)
                $flag = true;
                mysqli_close($conn);
            return $flag;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }

    function getBckSizes(){
        try{
            $conn = getConn();
            $sql = "SELECT * FROM backlog_size ORDER BY bck_points ASC";
            $result = mysqli_query($conn, $sql);
            $bckSizeList = array();
            $count = mysqli_num_rows($result);
            if($count > 0){
                for($x=0; $x < $count; $x++){
                    $row = mysqli_fetch_assoc($result);
                    $itm = new Backlog_Size();
                    $itm->bck_size = $row['bck_size'];
                    $itm->bck_points = $row['bck_points'];
                    $bckSizeList[$x] = $itm;
                }
            }
            return $bckSizeList;
        } catch(mysqli_sql_exception $e){
            $err = new Err();
            $err->errMsg = $e->getMessage();
            return $err;
        }
    }
?>