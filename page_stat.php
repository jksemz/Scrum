
<?php
session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    echo '<!DOCTYPE HTML>';
    include_once 'hub.php';
    includeJS();
    $user = new User();

/*
    Filename: page_member.php
    Author: Jethro Kyle Sempio
    
    Script Type: User Interface
    Description: 
    Scrum project member dashboard
*/

    if(isset($_SESSION['userid']) && isset($_GET['projid'])){
        if(validateProjMember($_SESSION['userid'], $_GET['projid']) || validateProjOwner($_SESSION['userid'], $_GET['projid'])){
            $user = getUserRoot($_SESSION['userid']);
            $members = getProjMembers($_GET['projid']);
            array_push($members, getProjOwner($_GET['projid']));
            $proj = getProj($_GET['projid']);

            /*
            for($x=0; $x < count($members); $x++){
                if($members[$x]->user_id == $user->user_id){
                    unset($members[$x]);
                    $members = array_values($members);
                    break;
                }
            }*/
        }
        else
            header('location:index.php'); 
    }
    else
        header('location:index.php'); 

    if(isset($_POST['backProj'])){
        header('location:page_proj.php?projid='.$proj->proj_id);
    }
?>
<html>
    <head>
        <title>Scrum</title>
    </head>
    <body>
        <?php 
            navBar();
        ?>
        <br>
        <script>
            function makeBurnChart(userid, projid, s_startDate, s_endDate, maxPoints, proj_numWeeks, elemid){
                let date = new Date(s_startDate);
                let endDate = new Date(s_endDate);
                let today = new Date('<?php echo getToday(); ?>');

                let idealBurn = maxPoints / (proj_numWeeks * 7);

                var data = {
                labels: [],
                series: [[],[]]
                };

                var options = {
                width: 1000,
                height: 140
                };
                
                var ajax = new XMLHttpRequest();
                while(date.getTime() <= endDate.getTime()){
                    data.labels.push((date.getMonth()+1)+'/'+date.getDate());
                    data.series[0].push(maxPoints);
                    maxPoints -= idealBurn;
                    if(date.getTime() <= today.getTime()){
                        let dateString = date.getFullYear() +'-'+ (date.getMonth()+1) +'-'+ date.getDate();
                        let todayString = today.getFullYear() +'-'+ (today.getMonth()+1) +'-'+ today.getDate();
                        let getstring = "?userid="+userid+"&projid="+projid+"&date="+dateString;
                        ajax.open("GET", "util_ajax.php"+getstring, false);
                        ajax.send(null);   
                        let pts = ajax.responseText;
                        data.series[1].push(pts);         
                        console.log("date:"+dateString+" today:"+todayString+" pts:"+pts);
                    }
                    date.setTime(date.getTime() + 86400000);
                }
                new Chartist.Line(elemid, data, options);
            }

            function pieSummary(bckDone, bckTotal){
                let data = {
                    series:[]
                }
                let percentage = bckDone / bckTotal * 100;
                data.series.push(percentage);
                data.series.push(100 - percentage);

                let options = {
                donut: true,
                donutWidth: 45,
                donutSolid: true,
                startAngle: 0,
                showLabel: false,
                width:250,
                height:250
                }

                var options2 = {
                width: 1000,
                height: 200
                };
                new Chartist.Pie('#pieSm', data, options);
            }
        </script>
            <div class='sc-white' style='margin:30px;height:550px;'>
                <form method='POST'>
                    <button name='backProj' class='w3-button w3-green'>< Back to Project</button>     
                </form>   
                <div style='padding:20px'>
                    <div class='w3-half'>
                        <h2>Summary</h2>
                        <div class='w3-half'>
                            <?php
                                    $bckDone = projBckDoneCount($_GET['projid']);
                                    $bckTotal = projBckCount($_GET['projid']);
                                    echo "<p>";
                                    echo (round($bckDone / $bckTotal * 100));
                                    echo "% Done </p>";
                                    echo "<p>Total Backlog: $bckDone / $bckTotal</p>";
                                ?>
                            <div id='pieSm' class='ct-chart ct-perfect-fourth'></div>
                        </div>
                        <div class='w3-half'>
                            <p>Project Started: <?php echo $proj->startDate; ?></p>
                            <p>Deadline: <?php echo getProjEndDate($_GET['projid']); ?></p>
                            <p id='daysLeft'></p>
                            <script>
                                var today = new Date('<?php echo getToday(); ?>');
                                var endDate = new Date('<?php echo getProjEndDate($_GET['projid']); ?>');
                                document.getElementById('daysLeft').innerHTML = ((endDate.getTime() - today.getTime()) / 86400000) + " days left";
                            </script>
                        </div>
                            
                         <script>
                                pieSummary(<?php echo $bckDone ?>, <?php echo $bckTotal ?>);
                        </script>
                    </div>
                    <div class='w3-half'>
                        <h2>Burn Down Chart</h2>
                        <div style='height:400px;overflow-y:scroll'>
                            <?php 
                                for($x=0; $x < count($members); $x++){
                                    $mxPts = userMaxPoints($members[$x]->user_id, $_GET['projid']);
                                    echo "<h3>".$members[$x]->name."</h3>";
                                    echo "<div style='width:100%;overflow-x:scroll'>";
                                        echo "<div class='ct-chart' id='brn$x'></div>";
                                    echo "</div>";

                                    $itm = $members[$x];
                                    $endDate = getProjEndDate($_GET['projid']);
                                    echo "<script>";
                                        echo "makeBurnChart($itm->user_id, $proj->proj_id, '$proj->startDate', '$endDate', $mxPts, $proj->proj_numWeeks, brn$x);";
                                    echo "</script>";
                                } 
                            ?>
                        </div>
                    </div>    
                </div>
                
                    <!--
                    <div style='width:400px;overflow-x:scroll'>
                        <div id='tst' class='ct-chart'></div>
                    </div>  -->
            </div>
    </body>
</html>