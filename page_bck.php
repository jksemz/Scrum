<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'hub.php';
$user = new User();

/*
Filename: page_bck.php
Author: Jethro Kyle Sempio

Script Type: User Interface
Description: 
Vie backlog UI
*/
    $bck = '';
    if(isset($_SESSION['userid']) && isset($_SESSION['projid']) && isset($_GET['bckid'])){
        if(validateProjMember($_SESSION['userid'], $_SESSION['projid']) || validateProjOwner($_SESSION['userid'], $_SESSION['projid'])){
            
            if(isset($_GET['done'])){
                backlogDone($_GET['done']);
                unset($_GET['done']);
            }
        
            if(isset($_GET['undone'])){
                backlogUndone($_GET['undone']);
                unset($_GET['undone']);
            }
            //$user = getUserRoot($_SESSION['userid']);
            $bck = getBacklog($_GET['bckid']);
            //$members = getProjMembers($_SESSION['projid']);
            $proj = getProj($_SESSION['projid']);
            //$bckSizes = getBckSizes();
            if($bck instanceof Err)
                echo $bck->errMsg;
        }
        else
            header('location:index.php'); 
    }
    else
        header('location:index.php'); 

    if(isset($_POST['backProj'])){
        unset($_SESSION['projid']);
        header('location:page_proj.php?projid='.$proj->proj_id);
    }

    if(isset($_POST['btnBckEdit']))
        header('location:page_bckEdit.php?bckid='.$bck->bck_id);
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Scrum</title>
    </head>
    <?php
        navBar();
    ?>
    <body style='overflow:hidden'>
        <div class='w3-card sc-white' style='margin:50px;height:100%'>
            <form method='POST'>
                <button name='backProj' class='w3-button w3-green'>< Back to Project</button>
                <button name='btnBckEdit' class='w3-button w3-green' style='margin-left:30px;float:right'>Edit Backlog</button>
            <div style='padding:30px;height:65%;'>
                <h2>Backlog</h2>
                <br>
                <div class='w3-half'>
                    <p>Backlog Title</p>
                        <p><?php echo $bck->bck_title; ?></p>
                    <br><br>

                    <p>Description</p>
                    <textarea readonly name='i_bckDesc' rows='5' cols='25' style='sc-white sc-txt-drkgrey'><?php echo $bck->bck_desc; ?></textarea>
                    <br><br>

                    <p>Backlog Size</p>
                        <p><?php echo $bck->bck_size; ?></p>
                </div>

                <div class='w3-half'>
                    <p>Assigned User</p>
                        <p>
                            <?php 
                                if($bck->user_id == null)
                                    echo "None assigned";
                                else
                                    echo getUser($bck->user_id)->name;
                            ?>
                        </p>

                    <p>Sprint Number</p>
                        <p><?php echo $bck->sprint_no+1; ?></p>
                    
                    <br>
                    <input type='checkbox' id='isDone' onchange='toggleIsDone(this)'>
                    <span id='lbl_done' style='font-size:18px'>Done</span>
                    <br>
                    <?php
                        if($bck->status == 'D'){
                            echo "<p>Date Finished</p>";
                            echo "<p>$bck->done_date</p>";
                        }
                    ?>

                    <script>
                        var checkbox = document.getElementById('isDone');
                        var bckid = <?php echo $bck->bck_id; ?>;
                        var isDone = <?php if($bck->status == 'D') echo 1; else echo 0; ?>;
                        var isAssigned = <?php if($bck->user_id == NULL) echo 0; else echo 1; ?>;

                        if(isDone == 1)
                            checkbox.checked = true;
                        
                        if(isAssigned != 1){
                            checkbox.disabled = true;
                            document.getElementById('lbl_done').style.color = 'grey';
                        }

                        function toggleIsDone(elem){
                            if(elem.checked)
                                window.location = "page_bck.php?bckid="+bckid+"&done="+bckid;
                            else
                                window.location = "page_bck.php?bckid="+bckid+"&undone="+bckid;
                        }
                    </script>
                </div>
            </div>
            <br><br>
            </form>
        </div>
    </body>
</html>