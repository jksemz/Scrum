<?php 
    function projList($projList){
        echo '<div style="margin-top:10px;margin-bottom:10px;">';
        if(!($projList instanceof Err)){
            for($x=0; $x < count($projList); $x++){
                projCard($projList[$x]);
            }
        } 
        else
            echo $projList->errMsg;
        echo '</div>';
    }
 ?>

 <?php
    function projCard($projItem){ ?>
    <a href='<?php echo "page_proj.php?projid=".$projItem->proj_id ?>' style='text-decoration:none'>
        <div class='w3-card sc-green' style='
            width:200px; 
            margin:10px;
            height:150px;
            display:inline-table;
            padding:10px;
            color:white;
            position:relative
        '>
            <p style='overflow:hidden;white-space:no-wrap'>
                <?php echo $projItem->proj_name; ?>    
                <br>
                By: <?php echo getUser($projItem->user_id)->name; ?>
            </p>
            
            <div style='position:absolute;right:0;bottom:0;margin:5px'>
                <span class='w3-dropdown-hover w3-green'>
                    <img src='3rd_party/img/ic_settings_white_24dp.png'>
                    <span class='w3-dropdown-content sc-white w3-card'>
                        <form method='POST'>
                            <?php 
                                if(validateProjOwner($_SESSION['userid'], $projItem->proj_id)){
                                    echo "<button name='btnEdit' value='$projItem->proj_id' class='w3-button' style='width:100%'>Edit</buton>";
                                    echo "<button name='btnDel' value='$projItem->proj_id' class='w3-button' style='width:100%'>Delete</buton>";
                                } else
                                    echo "<button name='btnKick' value='$projItem->proj_id' class='w3-button' style='width:100%'>Leave</buton>";
                            ?>
                            
                        </form>
                    </span>
                </span>
            </div>
        </div>
    </a>
<?php
    }
 ?>

<?php
    function inviteList($inviteList){
        for($x = 0; $x < count($inviteList); $x++)
            inviteCard($inviteList[$x]);
    }
?>

 <?php
    function inviteCard($inviteItem){ ?>
        <div class='w3-bar-item sc-grey' style='margin-bottom:10px;margin-right:100px'>
            <?php echo getProjOwnerName($inviteItem->proj_id) ?> invited you to this project: 
            <br>
            <?php echo getProjName($inviteItem->proj_id) ?>
            <br>
            <button name='btnInvAc' value='<?php echo $inviteItem->invite_id ?>' class='w3-button w3-teal'>ACCEPT</button>
            <button name='btnInvRj' value='<?php echo $inviteItem->invite_id ?>' class='w3-button w3-red'>REJECT</button>
        </div>
<?php
    }
?>

<?php
    function bckList($user, $bckList){ ?>
        <div class='w3-card-2 sc-green' style='
        display:inline-table;
        padding-left:10px;
        padding-top:10px;
        margin:10px;
        width:250px;
        height:450px
        '>
            <img src='3rd_party/img/ic_account_circle_white_48dp.png' style='display:inline'>
            <h5 style='display:inline' class='w3-text-white'>
            <?php
                if($user->user_id == $_SESSION['userid'])
                    echo 'You';
                else
                    echo $user->name;
            ?>
            </h5>
            <div style='overflow-y:scroll; overflow-x:hidden; height:430px; margin-top:5px'>
            <?php
                for($x = 0; $x < count($bckList); $x++)
                    bckCard($bckList[$x]);
            ?>
            </div>
        </div>
<?php
    }
?>

<?php
    function bckCard($bckItem){ ?>
        <a href='<?php echo "page_bck.php?bckid=".$bckItem->bck_id; ?>' style='text-decoration:none'>
            <div class='w3-card sc-white' style='
                height:85px;
                margin-bottom:5px;
                padding:10px;
            '>
                <span style='font-size:18px padding-bottom:-50px;'> <?php echo $bckItem->bck_title; ?> </span>
                <br>
                <span style='font-size:12px'> Size: <?php echo $bckItem->bck_size; ?> </span>
                <br>
                <?php
                    if($bckItem->status == 'D'){
                        echo "<img src='3rd_party/img/ic_check_circle_black_18dp.png'>";
                        echo "<span style='font-size:12px;color:green;margin-left:3px'>".$bckItem->done_date."</span>";
                    }
                ?>
            </div>
        </a>
<?php
    }
?>

<?php
    function userCard($userItem, $projid){ ?>
        <div class='w3-card w3-white' style='padding-left:20px; margin-bottom:5px'>
            <div style='display:inline-table;margin-right:10px'>
                <img src='3rd_party/img/ic_account_circle_black_48dp.png'>
            </div>
            <div style='display:inline-table;width:70%'>
                <p style='margin-bottom:-5px;'>
                    <?php
                        echo $userItem->name;
                        $kickable = true;
                        if($userItem->user_id == getProjOwner($projid)->user_id){
                            echo '(Master)';
                            $kickable = false;
                        }
                        if($userItem->user_id == $_SESSION['userid'])
                            echo '(Me)';
                    ?>
                </p>
                <span class='sc-txt-drkgrey' style='font-size:12px'>
                    <?php
                        echo $userItem->email;
                    ?>
                </span>
            </div>
            <?php
                if($kickable){ ?>
                    <div style='display:inline-table'>
                        <button name='btnKick' value='<?php echo $userItem->user_id ?>' class='w3-button w3-green'>KICK</button>
                    </div>
            <?php
                }
            ?>
        </div>
<?php
    }
?>  