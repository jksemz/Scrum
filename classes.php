<?php
    class User{
        var $user_id = 0;
        var $name = '';
        var $email = '';
        var $pwd = '';
    }

    class Proj{
        var $proj_id = 0;
        var $user_id = 0;
        var $proj_name = '';
        var $proj_numWeeks = 0;
        var $startDate = '00-00-0000';
        var $sprint_count = 0;
        var $sprint_numWeeks = 0;
    }

    class Proj_Invite{
        var $invite_id = 0;
        var $proj_id = 0;
        var $user_id = 0;
        var $status = '';
    }

    class Backlog{
        var $bck_id = 0;
        var $proj_id = 0;
        var $user_id = 0;
        var $bck_size = '';
        var $bck_title = '';
        var $bck_desc = '';
        var $sprint_no = -1;
        var $status = '';
        var $done_date = '00-00-0000';
    }

    class Backlog_Size{
        var $bck_size = '';
        var $bck_points = 0;
    }

    class Err{
        var $errMsg = '';
    }
?>