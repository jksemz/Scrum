<?php
/*
    Filename: hub.php
    Author: Jethro Kyle Sempio
    
    Script Type: Hub
    Description: 
    Central link to common dependencies
*/
    // Hub.php - includes common dependencies
    include_once 'widget_nav.php';
    include_once 'widget_card.php';
    include_once 'util_db.php';
    include_once 'classes.php';

    function includeJS(){ ?>
        <script src="3rd_party/js/chartist-js-develop/dist/chartist.min.js"></script>
        <script src="3rd_party/js/jquery-3-min.js"></script>
<?php
    }
?>

<!-- CSS Dependencies -->

<link href="3rd_party/css/chartist.min.css" rel="stylesheet" type="type/css" />
<link rel="stylesheet" type="text/css" href="3rd_party/css/w3.css"/>
<link rel="stylesheet" type="text/css" href="ui_custom.css"/>
<meta charset="utf-8">
