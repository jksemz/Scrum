DELIMITER $$
DROP PROCEDURE IF EXISTS sendInvite;
CREATE PROCEDURE sendInvite(IN i_email VARCHAR(30), IN i_projid INT(8))
BEGIN
	DECLARE r_userid    INT(8);
    DECLARE isInvited   INT(1);
    DECLARE isMembered  INT(1);
    DECLARE isOwner     INT(1);
    
    SET r_userid = (SELECT user_id FROM user 
    	WHERE email = i_email);
    SET isInvited = (SELECT COUNT(user_id) FROM proj_invite 
    	WHERE user_id = r_userid AND proj_id = i_projid AND status = 'O');
    SET isMembered = (SELECT COUNT(user_id) FROM proj_member
    	WHERE user_id = r_userid AND proj_id = i_projid);
    SET isOwner = (SELECT COUNT(user_id) FROM proj
        WHERE user_id = r_userid AND proj_id = i_projid);
    
    IF isInvited = 0 AND isMembered = 0 AND isOwner = 0 THEN
		INSERT INTO proj_invite
        	(
                user_id		,
                proj_id 	,
                status
            )
            VALUES
            (
                r_userid	,
                i_projid	,
                'O'
            );
    END IF;	
END $$

-- SQL Function
DROP PROCEDURE IF EXISTS rejectInvite;
CREATE PROCEDURE rejectInvite(IN i_inviteid INT(8))
BEGIN
    DELETE FROM proj_invite WHERE invite_id = i_inviteid;
END;

-- SQL Function
DROP PROCEDURE IF EXISTS kickMember;
CREATE PROCEDURE kickMember(IN i_userid INT(8), IN i_projid INT(8))
BEGIN
    UPDATE backlog SET user_id = NULL WHERE user_id = i_userid AND proj_id = i_projid;
    DELETE FROM proj_member WHERE user_id = i_userid AND proj_id = i_projid;
END;

DELIMITER $$
DROP PROCEDURE IF EXISTS taskDone;
CREATE PROCEDURE taskDone(IN i_bckid INT(8))
BEGIN
    UPDATE backlog SET status = 'D', done_date = current_date() WHERE bck_id = i_bckid AND user_id IS NOT NULL;
END $$

DROP PROCEDURE IF EXISTS taskUndone;
CREATE PROCEDURE taskUndone(IN i_bckid INT(8))
BEGIN
    UPDATE backlog SET status = 'O', done_date = NULL WHERE bck_id = i_bckid;
END $$

DELIMETER $$
DROP PROCEDURE IF EXISTS projBckCount;
CREATE PROCEDURE projBckCount(IN i_projid INT(8))
BEGIN
    SELECT COUNT(bck_id) as 'bckCount' FROM backlog WHERE proj_id = i_projid;
END $$

DELIMETER $$
DROP PROCEDURE IF EXISTS projBckDoneCount;
CREATE PROCEDURE projBckDoneCount(IN i_projid INT(8))
BEGIN
    SELECT COUNT(bck_id) as 'bckCount' FROM backlog WHERE proj_id = i_projid AND status = 'D';
END $$

DELIMETER $$ ;
DROP PROCEDURE IF EXISTS userMaxPoints;
CREATE PROCEDURE userMaxPoints(IN i_userid INT(8), IN i_projid INT(8), OUT o_maxPoints INT(8))
BEGIN
    SELECT SUM(backlog_size.bck_points) FROM backlog
INNER JOIN backlog_size ON backlog.bck_size = backlog_size.bck_size
    WHERE user_id = i_userid AND proj_id = i_projid INTO o_maxPoints;
END $$

DELIMETER $$ ;
DROP PROCEDURE IF EXISTS userDonePoints;
CREATE PROCEDURE userDonePoints(IN i_userid INT(8), IN i_projid INT(8))
BEGIN
    SELECT SUM(backlog_size.bck_points) FROM backlog
INNER JOIN backlog_size ON backlog.bck_size = backlog_size.bck_size
    WHERE user_id = i_userid AND proj_id = i_projid AND status = 'D';
END $$

DELIMETER $$ ;
DROP PROCEDURE IF EXISTS getToday;
CREATE PROCEDURE getToday()
BEGIN
    SELECT current_date as 'today';
END $$

DELIMETER $$ ;
DROP PROCEDURE IF EXISTS getEndDate;
CREATE PROCEDURE getEndDate(IN i_projid INT(8))
BEGIN
    SELECT DATE_ADD(start_date, INTERVAL proj_numWeeks WEEK) as 'end_date' FROM proj WHERE proj_id = i_projid;
END $$

DELIMETER $$ ;
DROP PROCEDURE IF EXISTS pointsLeftOnDate;
CREATE PROCEDURE pointsLeftOnDate(IN i_userid INT(8), IN i_projid INT(8), IN i_date VARCHAR(10))
BEGIN
    DECLARE points INT(8);
    SET points = (SELECT SUM(backlog_size.bck_points) + 0 FROM backlog
    INNER JOIN backlog_size ON backlog.bck_size = backlog_size.bck_size
    WHERE 
        status = 'D' AND
        done_datre <= STR_TO_DATE(i_date,'%Y-%m-%d') AND
        user_id = i_userid AND
        proj_id = i_projid);
    CALL userMaxPoints(i_userid,i_projid,@maxPoints);
    IF points IS NULL THEN
        SELECT @maxPoints AS 'pointsLeft';
    END IF;
    IF points IS NOT NULL THEN
        SELECT @maxPoints - points AS 'pointsLeft';
    END IF;
END $$

DELIMETER $$ ;
DROP PROCEDURE IF EXISTS delProj;
CREATE PROCEDURE delProj(IN i_projid INT(8), IN i_userid INT(8))
BEGIN
    DECLARE isOwner INT(1);
    SET isOwner = (SELECT COUNT(user_id) FROM proj
        WHERE proj_id = i_projid AND user_id = i_userid);

    IF isOwner = 1 THEN
        DELETE FROM backlog WHERE proj_id = i_projid;
        DELETE FROM proj_member WHERE proj_id = i_projid;
        DELETE FROM proj_invite WHERE proj_id = i_projid;
        DELETE FROM proj WHERE proj_id = i_projid;
    END IF;
END $$