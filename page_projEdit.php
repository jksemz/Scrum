<!DOCTYPE HTML>
<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'hub.php';
/*
    Filename: page_projEdit.php
    Author: Jethro Kyle Sempio
    
    Script Type: User Interface
    Description: 
    UI for editing scrum project
*/

    if(isset($_SESSION['userid']) && isset($_GET['projid'])){
        if(validateProjMember($_SESSION['userid'], $_GET['projid']) || validateProjOwner($_SESSION['userid'], $_GET['projid'])){
            $user = getUserRoot($_SESSION['userid']);
            $oldProj = getProj($_GET['projid']);

        } else
            header('location:index.php'); 
    }
    else
        header('location:index.php'); 

    if(isset($_POST['btn_add'])){
        $proj = new Proj();
        $proj->proj_id = $_GET['projid'];
        $proj->user_id = $user->user_id;
        $proj->proj_name = $_POST['i_projName'];
        $proj->proj_numWeeks = $oldProj->proj_numWeeks;
        $proj->startDate = $oldProj->startDate;
        $proj->sprint_numWeeks = $oldProj->sprint_numWeeks;
        $proj->sprint_count = $oldProj->sprint_count;
        $err = editProj($proj);
        if($err instanceof Err){
            echo $err->errMsg;
            echo $proj->startDate;
        } else
            header('location:page_user.php');
    }
?>
    <html>
        <head>
            <title>Scrum</title>
        </head>
        <body>
            <?php 
                navBar();
            ?>
            <br>
            <div class='w3-card-2 w3-container sc-white' style ='margin:20px;padding:10px;'>
                <form method='POST'>
                    <h1>Edit Project</h1>    
                    <div class='w3-half'>
                        <span class='sc-txt-drkgrey'>Project Name:</span><br>
                        <input type='text' value='<?php echo $oldProj->proj_name; ?>' name='i_projName' class='sc-grey sc-txt-drkgrey sc-flat'style='padding:10px'>  
                        
                        <br>
                        
                        <span class='sc-txt-drkgrey'>Start Date:</span><br>
                        <input type='date' value='<?php echo $oldProj->startDate; ?>' id='i_startDate' name='i_startDate' onchange='dymTrap()' class='sc-grey sc-txt-drkgrey sc-flat'style='padding:10px' disabled> 
                        <br><br>

                        <span class='sc-txt-drkgrey'>Project Length(# of Weeks):</span><br>
                        <input type='number' value='<?php echo $oldProj->proj_numWeeks; ?>' id='i_projLen' name='i_projLen' onchange='dymTrap()' class='sc-grey sc-txt-drkgrey sc-flat'style='padding:10px; width:100px' disabled> 

                        <br><br>

                        <span class='sc-txt-drkgrey'>Sprint Length(# of Weeks):</span><br>
                        <input type='number' value='<?php echo $oldProj->sprint_numWeeks; ?>' id='i_sprLen' name='i_sprLen' onchange='dymTrap()' class='sc-grey sc-txt-drkgrey sc-flat'style='padding:10px; width:100px' disabled> 
                    </div>
                    <div class='w3-half'>
                    <h3>End Date:</h3>
                        <h4 id='finDate'>MMMM/DD/YY</h4>
                        <h3>Number of Sprints:</h3>
                        <h4 id='sprCount'>00</h4>
                        <br><br><br>
                    </div>
                    <br><br>
                    <button class='w3-button w3-green' style='float:right;' name='btn_add'>Save Changes</button>
                </form>

                <script>
                    let today = new Date();
                    let today_string = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    
                    //document.getElementsByName('i_projName')[0].value = 'Untitled Project (' + today_string + ')';
                    dymTrap();
                    
                    function dymTrap(){
                        let i_startDate = document.getElementById('i_startDate');
                        let i_projLen = document.getElementById('i_projLen');
                        let i_sprLen = document.getElementById('i_sprLen');
                        let i_finDate = document.getElementById('finDate');
                        let i_sprCount = document.getElementById('sprCount');

                        let startDate = new Date(i_startDate.value);
                        let end = new Date(startDate.getTime());
                        let startDate_string = startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();

                        let timeDiff = startDate.getTime() - today.getTime(); 

                        if(i_projLen.value < 1){
                            i_projLen.value = 1;
                        }

                        if(i_sprLen.value < 1){
                            i_sprLen.value = 1;
                        }
                        
                        // Trap start date input before current date
                        // set end date based on start date and project length
                        /*
                        if(timeDiff >= 0 || (today_string == startDate_string)){
                            

                        } else{
                            //i_startDate.value = today_string;
                            startDate_string = today_string;
                            startDate.setTime(today.getTime());
                            end.setTime(startDate.getTime());
                            end.setTime(end.getTime() + (i_projLen.value * (604800 * 1000)));
                        } */
                        end.setTime(end.getTime() + (i_projLen.value * (604800 * 1000)));
                        i_finDate.innerHTML = (end.getMonth()+1) + "-" + end.getDate() + "-" + end.getFullYear();

                        // Trap sprint length greater than or not divisible by project length
                        if((i_projLen.value < i_sprLen.value) || (i_projLen.value % i_sprLen.value != 0)){
                            i_sprLen.value = 1;
                        }

                        i_sprCount.innerHTML = i_projLen.value / i_sprLen.value;
                    }
                </script>
            </div>
        </body>
    </html>